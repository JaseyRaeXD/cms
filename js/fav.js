//------------- forms-advanced.js -------------//
$(document).ready(function() {

    //------------- Fancy select -------------//
    $('.fancy-select').fancySelect();
    //custom templating
    
	//------------- Select 2 -------------//
    $('.sel-export').select2({
        placeholder: 'Export As...',
        minimumResultsForSearch: Infinity
    });
    
    
    $('#export-as').click(function(e) {
//        e.preventDefault();  //stop the browser from following
//        window.location.href = 'sample/export-sample.txt';
//        window.location.assign('sample/export-sample.txt');
        window.open('./sample/export-sample.zip', 'Download');
    });

});