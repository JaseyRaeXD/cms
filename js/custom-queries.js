//------------- tables-data.js -------------//
$(document).ready(function() {

     //------------- Select 2 -------------//
    
    $('.sel-dash').select2({placeholder: 'Add to Dashboard'});
    
    $('.add-to-dash').click(function(){
        
        
        var queryName = $(this).parent().closest('tr').children('td').clone()	//clone the element
		.children()	//select all the children
		.remove()	//remove all the children
		.end()	//again go back to selected element
		.text();
        
        var dash = $(this).parent().find('.sel-dash :selected').text();  
        var dashval = $(this).parent().find('.sel-dash :selected').val();
    
        //make sure something is selected
        if (dashval != 0){
            //success notice for adding a query to a dashboard
            $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: 'Cool!',
                // (string | mandatory) the text inside the notification
                text: 'You can now see your ' + queryName + ' query on your ' + dash + ' dashboard!',
                // (int | optional) the time you want it to be alive for before fading out
                time: '',
                // (string) specify font-face icon  class for close message
                close_icon: 'l-arrows-remove s16',
                // (string) specify font-face icon class for big icon in left. if are specify image this will not show up.
                icon: 'ion-ios-speedometer-outline',
                // (string | optional) the class name you want to apply to that specific message
                class_name: 'success-notice'     

            });          
        }
        
    });
   
	
});