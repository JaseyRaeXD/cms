//------------- rules.js -------------//
$(document).ready(function() {
    
    var rtcTemplate = 
        {
            group: '<dl id="{{= it.group_id }}" class="rules-group-container">   <dt class="rules-group-header">     <div class="btn-group pull-right group-actions">       <button type="button" class="btn btn-xs blue white-text hoverable" data-add="rule">         <i class="{{= it.icons.add_rule }}"></i> {{= it.lang.add_rule }}       </button>       {{? it.settings.allow_groups===-1 || it.settings.allow_groups>=it.level }}         <button type="button" class="btn btn-xs blue white-text hoverable" data-add="group">           <i class="{{= it.icons.add_group }}"></i> {{= it.lang.add_group }}         </button>       {{?}}       {{? it.level>1 }}         <button type="button" class="btn btn-xs red white-text hoverable" data-delete="group">           <i class="{{= it.icons.remove_group }}"></i> {{= it.lang.delete_group }}         </button>       {{?}}     </div>     <div class="btn-group group-conditions">       {{~ it.conditions: condition }}         <label class="btn btn-xs gray-light white-text hoverable" style="z-index:2;">           <input type="radio" name="{{= it.group_id }}_cond" value="{{= condition }}"> {{= it.lang.conditions[condition] || condition }}         </label>       {{~}}     </div>     {{? it.settings.display_errors }}       <div class="error-container"><i class="{{= it.icons.error }}"></i></div>     {{?}}   </dt>   <dd class=rules-group-body>     <ul class=rules-list></ul>   </dd> </dl>',
            
            rule: '<li id="{{= it.rule_id }}" class="rule-container">   <div class="rule-header">     <div class="btn-group pull-right rule-actions">       <button type="button" class="btn btn-xs red white-text hoverable" data-delete="rule">         <i class="{{= it.icons.remove_rule }}"></i> {{= it.lang.delete_rule }}       </button>     </div>   </div>   {{? it.settings.display_errors }}     <div class="error-container"><i class="{{= it.icons.error }}"></i></div>   {{?}}   <div class="rule-filter-container"></div>   <div class="rule-operator-container"></div>   <div class="rule-value-container"></div> </li>',
            
            filterSelect: '{{ var optgroup = null; }} <select class="form-control" name="{{= it.rule.id }}_filter">   {{? it.settings.display_empty_filter }}     <option value="-1">{{= it.settings.select_placeholder }}</option>   {{?}}   {{~ it.filters: filter }}     {{? optgroup !== filter.optgroup }}       {{? optgroup !== null }}</optgroup>{{?}}       {{? (optgroup = filter.optgroup) !== null }}         <optgroup label="{{= it.translate(it.settings.optgroups[optgroup]) }}">       {{?}}     {{?}}     <option value="{{= filter.id }}">{{= it.translate(filter.label) }}</option>   {{~}}   {{? optgroup !== null }}</optgroup>{{?}} </select>'
            
        };

    //------------- QUERY BUILDER  -------------//
    var rules_basic = {
      condition: 'AND',
      rules: [{
        id: 'price',
        operator: 'less',
        value: 10.25
      }, {
        condition: 'OR',
        rules: [{
          id: 'category',
          operator: 'equal',
          value: 2
        }, {
          id: 'category',
          operator: 'equal',
          value: 1
        }]
      }]
    };

    $('#builder').queryBuilder({
      plugins: ['bt-tooltip-errors'],

      filters: [{
        id: 'name',
        label: 'Name',
        type: 'string'
      }, {
        id: 'category',
        label: 'Category',
        type: 'integer',
        input: 'select',
        values: {
          1: 'Books',
          2: 'Movies',
          3: 'Music',
          4: 'Tools',
          5: 'Goodies',
          6: 'Clothes'
        },
        operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null']
      }, {
        id: 'in_stock',
        label: 'In stock',
        type: 'integer',
        input: 'radio',
        values: {
          1: 'Yes',
          0: 'No'
        },
        operators: ['equal']
      }, {
        id: 'price',
        label: 'Price',
        type: 'double',
        validation: {
          min: 0,
          step: 0.01
        }
      }, {
        id: 'id',
        label: 'Identifier',
        type: 'string',
        placeholder: '____-____-____',
        operators: ['equal', 'not_equal'],
        validation: {
          format: /^.{4}-.{4}-.{4}$/
        }
      }],
        
      icons: {
        add_group: "ion-android-funnel",
        add_rule: "ion-plus",
        remove_group: "ion-trash-b",
        remove_rule: "ion-trash-b",
        error: "ion-alert-circled"
      },
        
      templates: rtcTemplate,
        
//      template: {
////        group: function(group_id){
////            return '<dl id="{{= it.group_id }}" class="rules-group-container">   <dt class="rules-group-header">     <div class="btn-group pull-right group-actions">       <button type="button" class="btn btn-xs blue white-text hoverable" data-add="rule">         <i class="{{= it.icons.add_rule }}"></i> {{= it.lang.add_rule }}       </button>       {{? it.settings.allow_groups===-1 || it.settings.allow_groups>=it.level }}         <button type="button" class="btn btn-xs blue white-text hoverable" data-add="group">           <i class="{{= it.icons.add_group }}"></i> {{= it.lang.add_group }}         </button>       {{?}}       {{? it.level>1 }}         <button type="button" class="btn btn-xs red white-text hoverable" data-delete="group">           <i class="{{= it.icons.remove_group }}"></i> {{= it.lang.delete_group }}         </button>       {{?}}     </div>     <div class="btn-group group-conditions">       {{~ it.conditions: condition }}         <label class="btn btn-xs gray-light white-text hoverable" style="z-index:2;">           <input type="radio" name="{{= it.group_id }}_cond" value="{{= condition }}"> {{= it.lang.conditions[condition] || condition }}         </label>       {{~}}     </div>     {{? it.settings.display_errors }}       <div class="error-container"><i class="{{= it.icons.error }}"></i></div>     {{?}}   </dt>   <dd class=rules-group-body>     <ul class=rules-list></ul>   </dd> </dl>';
////        },
////        rule: function(rule_id){
////            return '<li id="{{= it.rule_id }}" class="rule-container">   <div class="rule-header">     <div class="btn-group pull-right rule-actions">       <button type="button" class="btn btn-xs red white-text hoverable" data-delete="rule">         <i class="{{= it.icons.remove_rule }}"></i> {{= it.lang.delete_rule }}       </button>     </div>   </div>   {{? it.settings.display_errors }}     <div class="error-container"><i class="{{= it.icons.error }}"></i></div>   {{?}}   <div class="rule-filter-container"></div>   <div class="rule-operator-container"></div>   <div class="rule-value-container"></div> </li>';
////        },
////        filterSelect: function(filter){
////            return '{{ var optgroup = null; }} <select class="form-control" name="{{= it.rule.id }}_filter">   {{? it.settings.display_empty_filter }}     <option value="-1">{{= it.settings.select_placeholder }}</option>   {{?}}   {{~ it.filters: filter }}     {{? optgroup !== filter.optgroup }}       {{? optgroup !== null }}</optgroup>{{?}}       {{? (optgroup = filter.optgroup) !== null }}         <optgroup label="{{= it.translate(it.settings.optgroups[optgroup]) }}">       {{?}}     {{?}}     <option value="{{= filter.id }}">{{= it.translate(filter.label) }}</option>   {{~}}   {{? optgroup !== null }}</optgroup>{{?}} </select>';
////        },
////        operatorSelect: function(operators){
////            return '{{? it.operators.length === 1 }} <span> {{= it.lang.operators[it.operators[0].type] || it.operators[0].type }} </span> {{?}} {{ var optgroup = null; }} <select class="form-control {{? it.operators.length === 1 }}hide{{?}}" name="{{= it.rule.id }}_operator">   {{~ it.operators: operator }}     {{? optgroup !== operator.optgroup }}       {{? optgroup !== null }}</optgroup>{{?}}       {{? (optgroup = operator.optgroup) !== null }}         <optgroup label="{{= it.translate(it.settings.optgroups[optgroup]) }}">       {{?}}     {{?}}     <option value="{{= operator.type }}">{{= it.lang.operators[operator.type] || operator.type }}</option>   {{~}}   {{? optgroup !== null }}</optgroup>{{?}} </select>';
////        }
//      },

      rules: rules_basic
    });
    
    //Style colors and icons of buttons auto generated from query builder 
    //had to go into query-builder.min.js to alter the icon options line 277
    $('.btn-success').addClass('blue hoverable');
    $('.glyphicon.glyphicon-plus').addClass('ion-plus');
    $('.glyphicon.glyphicon-plus-sign').addClass('ion-android-funnel');
    
    $('.btn-danger').addClass('red hoverable');
    $('.glyphicon.glyphicon-remove').addClass('ion-trash-b');

});