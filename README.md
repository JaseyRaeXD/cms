# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This tool was meant to help users organize, maintain, and create content. At different authority levels (admin, regular) users would be able to either create specific content, or manage business rules and other data. Users have the capability to advanced search, save often-used searches for quick access, and see the whole history of a component within the system. This should aid users with maintaining their internal data as well as allowing analysis of their entire library.

V2